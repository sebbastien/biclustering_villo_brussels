# Biclustering Villo! in Brussels

Bi-clustering algorithm to optimise the management of the bike fleet Villo! in Brussels, inspired from the paper:

"A novel spatio-temporal clustering technique to study the bike sharing system in Lyon."
By Galvani M., Torti A., Menafoglio A., Vantini S.

## Introduction
The paper write by Galvani et all in 2020 focus on a the bike sharing system (BSS) in Lyon, in France. BSS is short-term urban bicycle rental scheme that enable bicycles to be picked up at any self-serve bicycle station and returned to any other bicycle station. The aim is to developpe a bi-clustering algorithm with the purpose of providing useful information to optimise the management of the bike fleet by highlighting specific spatio-temporal patterns in the bike stations usage profiles. 

From each station, custumers can pickup and drop bike. By monitoring the loading profile of each station over weeks, it is possible to find patterns of full and/or empty station in order to optimise the reffill or the move of bike by the company. By knowing which day of the week some station are empty of full, the bike compagny can better planify the managment of the bike stations and avoid to loose rent or get unsastified cunstommer because there is no bike or place at the station.

## Get the data
Data are from the Open data API providided by the JCDecaux company which manage the bike in Brussels. From this API it is possible to get full report of each of the bike station with informations on the amount of avail bike, the number of bike stand, the GPS localisation of the station and more.

### Method:

1- Define your path and file name in the bash script get_villo_data.sh

2- Make sure that CURL and Python modules (json, csv, time, datetime) are installed.

3- Using crontab, run every hour the bash script get_villo_data.sh.  

-- After 7 full days, stop the crontab. --

4- Create for each bike stations a CSV file using all the previous JSON files, with houlry timestamp, including the variables: 'time', 'bike_stands', 'avail_bike', 'avail_stands', 'status', 'name'.

### Outputs:
The folder Villo/csv_data will contains a CVS file by hour including informations of all the bike stations.

The folder Villo/json_data will contains the original JSON files from the API requests.

Log_book file keep track of the error occuring during the crontab execution. 

## License
(c) Copyright 2021 Sebastien Le clec'h.

Licensed under the MIT license:

    http://www.opensource.org/licenses/mit-license.php



