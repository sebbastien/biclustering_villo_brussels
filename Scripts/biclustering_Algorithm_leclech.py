import os
from os import listdir
import os.path
import glob
import pandas as pd
import math
import numpy as np
import datetime
#%matplotlib inline
import matplotlib.pyplot as plt

def estimated_functional_data(matrix):
  ''' Compute the estimated functional data:
          following the equation: f_IJ_bar= mean_IJ + alpha_IJ + beta_IJ
           there are 4 steps  
  '''
  ## Step 2:
  # Compute the mean of matrix_masked
  mean_IJ = np.mean(matrix,axis=(0,1))
  # Compute the Alpha
  alpha_IJ = np.zeros((n,p))
  for Bk in range(0,n):
   sumCOLs = np.sum(matrix[Bk,:,:],axis=0)
   nb_cols = matrix[Bk,:,0].count()
   alpha_IJ[Bk] = (sumCOLs/nb_cols) - mean_IJ
  # Compute the Beta
  beta_IJ = np.zeros((m,p))
  for day in range(0,m):
   nb_rows = matrix[:,day,0].count()
   sumROWs = np.sum(matrix[:,day,:],axis=0)
   beta_IJ[day] = (sumROWs/nb_rows) - mean_IJ
  ## Step 2:
  # Put all the variable on the same dimenssion as the initial matrix
  alpha_IJ_big = np.zeros((n,m,p))
  beta_IJ_big = np.zeros((n,m,p))
  for day in range(0,m):
   alpha_IJ_big[:,day,:] = alpha_IJ
  for Bk in range(0,n):
   beta_IJ_big[Bk,:,:] = beta_IJ
  mean_big = np.zeros((n,m,p))
  for Bk in range(0,n):
   for day in range(0,m):
    mean_big[Bk,day,:] = mean_IJ
  ## Step 3
  # Compute the estimated functional data
  f_IJ_bar = mean_big+beta_IJ_big+alpha_IJ_big

  return f_IJ_bar


def get_H_score(f_IJ,f_IJ_bar) :
  ''' Compute the H_score of the full matrix:
  '''
  H_IJ = np.sum((f_IJ-f_IJ_bar)**2,axis=2)/p
  matrix_Hscore = np.mean(H_IJ)
  row_score = np.mean(H_IJ,axis=1)
  col_score = np.mean(H_IJ,axis=0)

  return matrix_Hscore,row_score,col_score


# Open the Matrix compute previously
matrixA = data = np.load('MatrixBike_spline_leclech.npy')

# Get the dimension of the Matrix: n=347, m=7, p=240
n,m,p = matrixA.shape
cluster_list= {}


# Create temp matrix
f_IJ_ref = matrixA*1
f_IJ_ref = np.ma.masked_equal(f_IJ_ref,0)

cluster_loop=1
end_loop_cluster = False

while end_loop_cluster == False :
 print('Algorithms start to find cluster %i'% (cluster_loop,) )
 #############################################################################
 ## Multi node deletion
 # Start from the full matrix:
 #   1- compute f_IJ_bar
 #   2- compute H-score
 #   3- Find the row which are lower than the matrix H-score scale by theta
 #   4- compute new f_IJ_bar without the rows found in step 3
 #   5- compute new H-score
 #   6- Find the col which are lower than the matrix H-score scale by theta
 #   7- compute new f_IJ_bar without the cols found in step 6
 #   8- compute new H-score
 #############################################################################
 ## Step 1
 f_IJ_bar = estimated_functional_data(f_IJ_ref)
 ## Step 2
 score_set = get_H_score(f_IJ_ref,f_IJ_bar)
 ## Step 3
 theta=1.25
 mask_cols = np.ma.greater(score_set[1], (score_set[0]*theta))
 # Put the col masked on the same dimenssion as the initial matrix
 f_IJ_new = f_IJ_ref*1
 for Bk in range(0,n):
  if mask_cols[Bk] == False :
   f_IJ_new[Bk,:,:] = 0
 f_IJ_new = np.ma.masked_equal(f_IJ_new,0)
 ## Step 4
 f_IJ_bar_r = estimated_functional_data(f_IJ_new)
 ## Step 5
 score_set_2 = get_H_score(f_IJ_new,f_IJ_bar_r)
 ## Step 6
 theta=1.25
 mask_cols = np.ma.greater(score_set_2[2], (score_set_2[0]*theta))
 ## Step 7
 f_IJ_new_2 = f_IJ_new*1
 for Bk in range(0,m):
  for day in range(0,m) :
   if mask_cols[day] == False :
    f_IJ_new_2[Bk,day,:] = 0
 f_IJ_new_2 = np.ma.masked_equal(f_IJ_new_2,0)
 ## Step 8
 f_IJ_bar_rc = estimated_functional_data(f_IJ_new_2)
 ## Step 9
 score_set_3 = get_H_score(f_IJ_new_2,f_IJ_bar_rc)

 print('    Multi node deletion',score_set[0],score_set_2[0],score_set_3[0])

 f_IJ_afterMND = np.ma.masked_equal(f_IJ_bar_rc,0)

 #############################################################################
 ## Single Node Deletion
 #   1- compute f_IJ_bar
 #   2- compute H-score
 #   3- remove col and row which allows to reduce the H-score until reaching the threshold delta.
 #   WARNING !!  TAKES LOT OF TIME !!
 #############################################################################
 ## Step 1
 f_IJ_bar = estimated_functional_data(f_IJ_afterMND)
 f_IJ_bar = np.ma.masked_equal(f_IJ_bar,0)
 ## Step 2
 score_set_init = get_H_score(f_IJ_ref,f_IJ_bar)
 ## Step 3
 delta = 0.005
 end_iter = False
 iter = 1
 while end_iter == False:
  H_IJ = np.zeros((n,m))
  for i in range(0,n):
   for j in range(0,m):
    # Compute the Hscore for each row and for each col
    H_IJ[i,j] = np.sum((f_IJ_ref[i,j]-f_IJ_bar[i,j])**2,axis=0)/p
    if np.isnan(H_IJ[i,j]) :
     H_IJ[i,j] = 0
  # Look for the position of the max H-score value and mask it in the matrix
  delRC = np.argwhere(H_IJ==H_IJ.max())
  f_IJ_bar[delRC[0][0],delRC[0][1]] = f_IJ_bar[delRC[0][0],delRC[0][1]] * 0
  f_IJ_bar = np.ma.masked_equal(f_IJ_bar,0)
  # Compute the H-score of the new matrix
  score_set = get_H_score(f_IJ_ref,f_IJ_bar)
  # Check if the new H-score is below the delta
  if score_set[0] < delta :
   end_iter = True
  else :
   end_iter = False
  iter= iter + 1
 
 print('     Single Node Deletion RUN',iter,score_set[0],f_IJ_bar.count())

 f_IJ_bar_end_MND = f_IJ_bar*1

 #############################################################################
 ## Single Node Addition
 #   1- compute H-score after the multiple node deletion
 #   2- Add element by element anc check if the H-score change with this new element.
 #   WARNING !!  TAKES LOT OF TIME !!
 #############################################################################
 ## Step 1
 score_set_init = get_H_score(f_IJ_ref,f_IJ_bar)
 ## Step 2
 f_IJ_bar_temp = f_IJ_bar*1
 iter = 0
 for i in range(0,n):
  for j in range(0,m):
   # Compute the Hscore for each row and for each col
   if H_IJ[i,j] == 0.0 :
    f_IJ_bar_temp[i,j] = f_IJ_afterMND[i,j]
    score_set_test = get_H_score(f_IJ_ref,f_IJ_bar_temp)
   if score_set_test[0] > score_set_init[0]:
    f_IJ_bar_temp[i,j] = f_IJ_bar[i,j]
    #print(score_set_test[0], score_set_init[0])
   else :
    iter = iter + 1
 print('    Single Node Addition RUN',iter, 'element have been added')

 f_IJ_bar_end_add = f_IJ_bar_temp*1

 #############################################################################
 ## Masking the found cluster
 #############################################################################
 # get mask of the used bike stations and days
 mask_cl1 = f_IJ_bar_end_add.mask
 # transpose the maske of used data to mask them in the orinigal data set
 mask_oposite=np.logical_not(mask_cl1)
 save_old_size = f_IJ_ref.count()
 f_IJ_ref.mask = mask_oposite
 save_new_size = f_IJ_ref.count()
 diff_masked = ((save_old_size-save_new_size)*100)/save_old_size

 #################################
 ## END of the first loop
 #################################
 #Save the list of the elemements:
 cluster_list[cluster_loop] = np.zeros((n,m))
 for i in range(0,n):
  for j in range(0,m):
   if np.ma.is_masked(f_IJ_bar_end_add[i,j]) == False :
    cluster_list[cluster_loop][i,j] = 1

 print('    Cluster %i represents %i pourcent of the data set with %i elements' %(cluster_loop, np.round(diff_masked),cluster_list[cluster_loop].sum()))

 ## Go the next loop to find the new cluster
 if cluster_list[cluster_loop].sum() > 0 :
  cluster_loop = cluster_loop + 1 
  end_loop_cluster = False
 else :
  end_loop_cluster = True




#####################################################
# Plot the loading profile acording to cluster_list
#####################################################
cluster_loop = 1
profile_bike = np.zeros((int(cluster_list[cluster_loop].sum()),240))
iter=0
for i in range(0,n):
  for j in range(0,m):
   if cluster_list[cluster_loop][i,j] == 1:
    profile_bike[iter] = matrixA[i,j]
    iter = iter+1
profile_mean = np.mean(profile_bike,axis=0)

X_plot_spline = np.linspace(0, 24, 240)
for iter in range(0,len(profile_bike)):
 plt.plot(X_plot_spline,profile_bike[iter])
plt.plot(X_plot_spline,profile_mean,lw='3')
plt.ylim(0,1)
