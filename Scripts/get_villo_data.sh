#!/bin/bash -l

cd $MY_home_PATH/Villo 

# save the ongoing date + time and create associated CSV file name
save_date=`date +"%m-%d_%H-%M"`
file_name=villo_$save_date
# API request for the bike stations report
curl -s GET 'https://api.jcdecaux.com/vls/v1/stations?contract=bruxelles&apiKey=94c2b832aa8bc3aad56d7d8be8f59bfb7dc91be7' > villo.json
# Convert the JSON file into CSV file for convinience
python convert_to_csv.py $file_name
# check if the CSV have been created and store it in specific folder. Write a logbook to keep track of missing API request.
if [ -f villo.json ] ; then
  echo JSON on $save_date      >> log_book
else
  echo [error] NO JSON on $save_date      >> log_book
fi
if [ -f $file_name.csv ] ; then
  echo CSV on $save_date      >> log_book
else
  echo [error] NO CSV on $save_date      >> log_book
fi
echo -----------------------------  >> log_book
mv villo.json json_data/$file_name.json
mv $file_name.csv csv_data