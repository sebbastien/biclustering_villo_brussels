# Python program to convert
# JSON file to CSV
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import json
import csv
import time
from datetime import datetime

 
file_name  = sys.argv[1]
now = datetime.now()
date_check = time.time() 

# Opening JSON file and loading the data
# into the variable data
with open('villo.json') as json_file:
    data = json.load(json_file)

# now we will open a file for writing
data_file = open(file_name+'.csv', 'w')
# create the csv writer object
csv_writer = csv.writer(data_file)
# Counter variable used for writing 
# headers to the CSV file
count = 0
for list_station in range(0,len(data)):
    if data[list_station]['last_update']/1000 < date_check :
      date_check = data[list_station]['last_update']
    if count == 0:
        # Writing headers of CSV file
        header = data[0].keys()
        csv_writer.writerow(header)
        count += 1
    # Writing data of CSV file
    csv_writer.writerow(data[list_station].values())


Last_update = datetime.fromtimestamp(date_check/1000).strftime("%c")
print('Script run on', now)
print('last station updated:',Last_update)
data_file.close()
